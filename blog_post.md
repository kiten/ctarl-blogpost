> ### Overall TODOs:
> - [ ] Add references to relevant Papers (Curi, Rothfuss, Jaksch)
> - [ ] Convert headers and paragraphs to HTML
> - [ ] Convert LaTeX equations to MathJax syntax
> - [ ] Clean up $t$ or $i$ for time index

# Introduction
Traditional reinforcement learning (RL) algorithms excel in learning policies tailored to specific tasks by maximizing cumulative rewards.
Most common approaches in control, such as model predictive control (MPC) or model-based reinforcement learning (MBRL), assume that the system dynamics $f^*(\cdot,\cdot)$ are known. Let's consider the standard optimal control (OC) problem and optimize for some specific cumulative reward $J(\cdot)$ or cost $C(\cdot)$ over a state trajectory horizon:

$$\pi^* = \argmin_{\pi \in \Pi} C(\pi, f^*) = \argmin_{\pi \in \Pi} \int_{0}^Tc(x, \pi(x)) \,dt$$
$$\text{s.t.} \quad \dot{x} = f^{*}(x, \pi(x))+w, \quad x(0) = x_0,$$

where $\pi^*$ denotes the optimal policy from a policy space $\Pi$ . Existing RL methods primarily rely on discrete-time dynamics $f^*_d(\cdot,\cdot)$. 
Commonly, this problem is discretized using approximate methods such as Euler-forward emulation::

$$\pi^* = \argmin_{\pi\in\Pi} C_d(\pi, f^*) = \argmin_{\pi\in\Pi} \sum_{t=0}^{T-1} \, c_d(x_t, \pi(x_t))$$
$$\text{s.t.}  \quad x_{t+1} = f^*_d(x_t, \pi(x_t)) + w_t, \quad x(0)= x_0$$

where $f^*_d(\cdot,\cdot)$ and $C_d(\cdot)$ denote the discretized dynamics and cost, respectively. 
Again, the goal is to find the optimal policy $\pi^*$. 

So, not only do many OC formulations assume that the system dynamics are known; many RL formulations assume that the system behaves in such a discrete manner and directly assign discrete rewards accordingly. 
However, in many real-world scenarios, the system is fundamentally continuous in time, and an accurate dynamics model $f^*$ is not available.

In this blog post, we highlight two recent contributions to the 2023 NeurIPS conference from our group, which adress the two issues of dealing with unknown system dynamics as well as leveraging the continuous-time aspect of real systems.

### Digression: System Identification
Identifying the unknown system dynamics by taking measurements is not a new field. Famously, Åström and Eyckhoff published a survey on system identification back in 1970 [reference]. 
In the traditional setting, parameters are estimated by measuring the system, which however requires some prior knowledge about the underlying equations of motion. 
Nowadays, the system dynamics are often learned non-parametrically from data, and RL has proven to be a flexible paradigm to learn about said dynamics and solve a task at hand using the cost function.


# Active Exploration in RL
At the heart of reinforcement learning lies the challenge of efficiently exploring and understanding an environment to optimize decision-making processes for given tasks. 
Traditional RL algorithms focus on optimizing cumulative rewards $C_d$ for specific tasks, often overlooking the global understanding of the system's dynamics. 
This results in directional bias, where the unknown system is mainly explored in potentially high-reward zones for the task at hand. 

How can we effectively learn the system dynamics in a general fashing in order to solve multiple different downstream tasks? We present an algorithm called OPAX, short for Optimistic Active Exploration. 
OPAX is designed to methodically explore unknown dynamical systems, enhancing the model's global comprehension of the system dyanamics.

### Optimistic and Greedy Exploration Strategies
OPAX builds upon our previous work on Hallucinated Upper-Confidence RL (H-UCRL) [reference], which propagates an optimistic approach to exploration. 
Opposed to greedy exploration, optimistic exploration strategies like H-UCRL or OPAX actively seek out areas of high uncertainty within the model. 

H-UCRL introduces a form of exploration that specifically targets the gaps in the model's knowledge, instead of exploiting the current best-known actions as a greedy approach would. 
This enables better long-term performance by ensuring a more comprehensive understanding of the environment, particularly in complex or penalty-based scenarios where greedy methods might otherwise lead to suboptimal exploration and learning outcomes. 
For a detailed exploration of these concepts, you can read [Felix Berkenkamp's blog post on the topic](https://berkenkamp.me/blog/2020-12-06-mbrl-exploration/).

### OPAX: An Optimistic Active Exploration Algorithm
We want to explore the dynamics of an unknown discrete-time system to learn the system dynamics $f_d^*$. 
To achieve this, OPAX plans an exploration policy $\pi_n$ to gather the most information possible about the system during each episode $n$. 
The planned trajectory targets state-action pairs where the model's epistemic uncertainty $\sigma_{n-1}$ is high, which naturally encourages exploration.

> Footnote: epistemic vs aleatoric uncertainty?

We study an episodic setting, with episodes $n=1,\ldots,N$. 
At the beginning of the episode $n$, we deploy the exploratory policy $\pi_n$, chosen from a policy space $\Pi$ for a horizon of $T$ on the system. 
Next, we obtain a trajectory $\tau_n = (x_{n,0},\ldots,x_{n,T})$, which we save to a dataset $\mathcal{D}_n$. 
This dataset contains all the transitions in a trajectory from a state-action pair $z_{n,i} = (x_{n,i}, \pi_n(x_{n,i}))$ to the respective next states $y_{n,i}= f_d^*(x_{n,i}, \pi_n(x_{n,i}))+w$ as follows:

$$\mathcal{D}_n = \{(\, z_{n,i} \,,\, y_{n,i}\,)_{0\leq i \le T} \}.$$

We use the collected data up to the current episode $\mathcal{D}_{1:n}$ (Note: OR $\mathcal{D}_{1:n-1}$) to learn an estimate $f_n$ of $f^*_d$. 
This in turn serves as a model to guide the exloration in the next episode. 
OPAX uses well-calibrated probabilistic models to quantify the epistemic uncertainty about the unknown dynamics: From our dataset $\mathcal{D}_{1:n}$ and due to epistemic uncertainty $\sigma_n$ (Note: and aleatoric uncertainty?), we receive a belief over $f_n$; a distribution over possible dynamics. 
More concretely, we build a set of all models that are compatible with the data collected up to the current episode. 
This set is defined as follows:

> Note: following eqn is simplified. Is this acceptable?

$$\mathcal{M_n}(\delta) = \{f \text{ s.t. } \forall z \in \mathcal{Z}: \quad\mid\mid\mu_{n}(z) - f(z)\mid\mid \, \le \beta_n(\delta) \sigma_{n}(z)\},$$

which is the set of all models $f$ within a confidence set spanned by $\mu_n$ and $\sigma_n$. 
The confidence level is given by an appropriate choice of the constants $\delta$ and $\beta_n(\delta)$. 
In other words, for a given state-action pair $z=(x,\pi(x))$, our learned model predicts a mean estimate $\mu_n(z)$ and quantifies our epistemic uncertainty $\sigma_n(z)$ about the function $f_d^*$ with a certain confidence. 

What is the cost function to drive the exploration with regard to this epistemic uncertainty $\sigma_n(z)$? In our paper, we relate the epistemic uncertainty to the principle of entropy reduction and information gain with regards to the unknown system dynamics. 
We interpret this as a reward $J$ as follows, which is what we want to optimize for:

$$\pi^*_n = \argmax_{\pi \in \Pi} \hspace{0.2em} J_n(\pi) = \argmax_{\pi \in \Pi} \hspace{0.2em} \mathbb{E}_{\tau^{\pi}}\left[\sum_{t=0}^{T-1} \sum^{d_x}_{j=1} \log \left(1 + \frac{\sigma_{n-1, j}^2(x_t, \pi(x_t))}{\sigma^2}\right)\right],\\x_{t+1} = f^{*}_d(x_t, \pi(x_t)) + w_t.$$

Opposed to a cost function, we now maximize this reward. 
Essentially, we take the sum of epistemic uncertainties $\sigma_{n-1,j}$ from our model of the previous episode $n-1$ over all state dimensions $j=1,\ldots,d_x$ and over our time horizon up to $T$. 
This is an OC problem with a tractable objective that also has a frequentist interpretation $-$ collect points with the highest epistemic uncertainty. 

However, the OC formulation above requires knowledge about $f_d^*$, which is unknown. 
A common choice is to use the mean estimator $\mu_{n-1}$ instead of $f_d^*$ for planning, which however is suceptible to model bias and is provably optimal only in the case of linear systems.

To this end, we propose using an optimistic planner instead, as suggested by our previous work on H-UCRL [reference]. 
Accordingly, given the mean estimator $\mu_{n-1}(z_t)$ and the epistemic uncertainty $\sigma_{n-1}(z_t)$, we solve the following optimal control problem

$$\pi^*_n = \arg \max_{\pi \in \Pi} \max_{\eta \in \Xi} \hspace{0.2em} J_n(\pi, \eta) = \argmax_{\pi \in \Pi} \hspace{0.2em} \mathbb{E}_{\tau^{\pi}}\left[\sum_{t=0}^{T-1} \sum^{d_x}_{j=1} \log \left(1 + \frac{\sigma_{n-1, j}^2(\hat{x}_t, \pi(\hat{x}_t))}{\sigma^2}\right)\right],\\\hat{x}_{t+1} = \mu_{n-1}(\hat{x}_t, \pi(\hat{x}_t)) + \beta_{n-1}(\delta)\cdot\sigma_{n-1}(\hat{x}_t, \pi(\hat{x}_t))\cdot\eta(\hat{x}_t) + w_t.$$

where $\Xi$ is the space of policies $\eta: \mathcal{X} \to [-1, 1]^{d_x}$.
Therefore, we use the additional optimization variable $\eta$ to hallucinate transitions that give us the most information. 
Overall, the resulting formulation corresponds to a simple optimal control problem with a larger action space, i.e., we increase the action space by another $d_x$ dimension. 
Intuitively, the policy $\pi_{n}$ that OPAX suggests behaves optimistically with respect to the information gain at each episode. 

### Theory
> Here comes some theory:
>
>Taking inspiration from Bayesian experiment design, we provide a comprehensive explanation for using model epistemic uncertainty as an intrinsic reward for exploration. 
> Mention mutual information --> information gain, Information gain is upper bounded by sum of epistemic uncertainties

How do we arrive at this reward structure? We start from the notion of mutual information, which quantifies the reduction in entropy around $f^*$ conditioned on our observations $y_n$. A common approach is to greedily pick a policy that maximizes the information gain $I$ conditioned on the previous observations at each episode:

$$\underset{\pi \in \Pi}{\max} \hspace{0.2em} \mathbb{E}_{\tau^{\pi}}\left[I\left(f^*_{\tau^{\pi}}; y_{\tau^{\pi}} \mid \mathcal{D}_{1:n-1}\right)\right].$$

Here, we maximize the information gain on $f^*_{\tau^\pi}$ based on our dataset up to the current episode $\mathcal{D}_{1:n-1}$ and our observations $y_{\tau^\pi}$ from the trajectory $\tau^\pi$ under the policy $\pi$ with respect to the process noise $w$.

By assuming Gaussian noise $w\sim\mathcal{N}(0,\sigma)$, we can simplify this objective. In our paper, we upper bound the Information gain by the sum of epistemic uncertainties as follows:
$$I\left(f^*_{\tau^{\pi}}; y_{\tau^{\pi}} \mid \mathcal{D}_{1:n-1}\right) \leq \frac{1}{2} \sum_{t=0}^{T-1} \sum^{d_x}_{j=1}\log \left(1 + \frac{\sigma_{n-1, j}^2(z_t)}{\sigma^2}\right).$$
The information gain is non-negative. Therefore, if the right-hand side goes to zero, then the $I$ on the left-hand side goes to zero as well. 

We study the convergence property of OPAX by first studying the regret of planning under unknown dynamics. Specifically, since we cannot evaluate the optimal exploration policy at the current episode $\pi^*_n$ and use the optimistic one, i.e. $\pi_n$ instead, we incur a regret, $J_n(\pi_n^*) - J_n(\pi_n)$. In our paper, we bound this regret by noting that it is proportional to the difference in the expected returns under the true dynamics and the optimistic planning under unknown dynamics. Hence, when we have low uncertainty in our predictions, planning optimistically suffers smaller regret.

First, we show that the algorithm converges for general Bayesian models, such as BNNs. We do this via the model complexity, which is given as follows:

$$\mathcal{MC}_N(f^*) := \underset{\mathcal{D}_1, \dots, \mathcal{D}_N \subset \mathcal{Z} \times \mathcal{X}}{\max}\sum^{N}_{n=1} \sum_{z \in \mathcal{D}_n} \mid\mid{\sigma_{n-1}(z)}\mid\mid^2_2.$$


The model complexity captures how difficult it is to learn $f^*$ given $N$ trajectories. Mainly, the more complicated $f^*$, the larger the epistemic uncertainties $\sigma_n$, and in turn, the larger corresponding $\mathcal{MC}_N(f^*)$. Moreover, if the model complexity measure is sublinear in $N$, i.e. $\mathcal{MC}_N(f^*)/N \rightarrow 0$ for $N\rightarrow \infty$, then the epistemic uncertainties also converge to zero in the limit, 

Ideally, as we increase $N$, i.e., collect more data to train our model, $\mathcal{MC}_N(f^*)$ increases sublinearly with $N$. Thus, as we use more data, our model epistemic uncertainty $\sigma_N(z)$ goes to zero, which implies convergence to the true function $f^*$.

We show that for general Bayesian systems, the maximum expected information gain can be bounded in terms of the model complexity measure:

$$\mathbb{E}_{\mathcal{D}_{1:N-1}}\left[\max_{\pi \in \Pi}\mathbb{E}_{\tau^{\pi}}\left[I\left(f^*_{\tau^{\pi}}; y_{\tau^{\pi}} \mid \mathcal{D}_{1:N-1}\right)\right]\right] \le \mathcal{O} \left(\beta_N T^{\frac{3}{2}}\sqrt{\frac{\mathcal{MC}_N(f^*)}{N}}\right)$$

If the right-hand side is monotonically decreasing with $N$, this guarantees that the information gain at episode $N$ is also shrinking with $N$, and the algorithm is converging. 

Besides that, we also show a frequentist interpretation and provide convergence guarantees for a rich class of dynamical systems. Unlike in the Bayesian setting, we can directly upper bound the epistemic uncertainty $\sigma_n$. By assuming that the functions $f^*_j$ for each state space dimension $j=1\ldots d_x$ lie in a Reproducing Kernel Hilbert Space (RKHS) with kernel $k$, we can also bound the model complexity with the maximum information gain $\gamma_N(k)$ introduced by Srivinas et al (reference). Then, with a probability of at least $1-\delta$, the sum of epistemic uncertainties is bounded by:

$$   \max_{\pi \in \Pi} \mathbb{E}_{\tau^{\pi}}\left[ \max_{z \in \tau^{\pi}}\sum^{d_x}_{j=1} \frac{1}{2}\sigma_{N, j}^2(z) \right] \le \mathcal{O} \left(\beta_N T^{\frac{3}{2}}\sqrt{\frac{{\gamma}_{N}(k)}{N}}\right)$$

Here, we model the system dynamics $f_N$ with a Gaussian Process (GP) with mean $\mu_N$ and posterior variance $\sigma_N$. The confidence level is given by $\beta_N(\delta)$. Moreover, if $\gamma_N(k)$ scales polylogarithmically in $N$, then the epistemic uncertainties $\sigma_{N,j}(z)$ go to $0$ as $N\to\infty$. 

To conclude, we can bound the information gain for general Bayesian systems, which guarantees convergence of the OPAX algorithm. For the frequentist case of RKHS dynamics, we established convergence guarantees for OPAX to the true function $f_d^*$ for a rich class of nunlinear dynamical systems. This all stems from the optimism paradigm, which shows its strength and crucial role in theoretical properties.

### Experiments
> Here's some text...


# The Need for Continuous-time Modeling
The conventional RL approach of using discrete-time dynamics and discretized rewards simplifies the modeling process and speeds up computation time. 
However, it introduces inaccuracies and even instability, especially when dealing with systems that exhibit complex, nonlinear behaviours over time. 
Thus, using discretized dynamics is actually limiting to many real-world applications.

Given the advantages of continuous-time modeling, we propose an optimistic continuous-time model-based RL algorithm – OCORL. 
OCORL has the ability to handle the dual challenge of deciding how to explore the environment, but at the same time when to make observations of the unknown underlying system.

Moreover, in the paper (?), we theoretically analyze OCORL and show a general regret bound that holds for any choice of measurement selection strategy (MSS). 
We further show that for common choices of MSSs, such as equidistant sampling, the regret is sublinear when we model the dynamics with GPs. 
>To our knowledge, we are the first to give a no-regret algorithm for a rich class of nonlinear dynamical systems in the continuous-time RL setting.

### The Measurement Selection Strategy (MSS)
> TODO: Explain what a MSS is. Mention state-action pair $z=(x,u)$.

We assume an episodic RL setting, meaning that at an episode $n$, we optimize for a given cost function $C(\pi_n,f_n)$. We not only want to optimize over the policy $\pi_n$ and then deploy it, but also want to find a plausible system $f_n$ which approximates the true system $f^*$.
In general, the true system dynamics for a continuous-time system are given by the following ODE:
$$\dot{x} = f^{*}(x, \pi(x))+w, \quad x(0) = x_0,$$

which we want to approximate with $f_n(x,\pi_n(x))\approx\dot{x}$. 
After every episode, we reset to $x_n(0)=x_0$. 

Now, how do we approximate the unknown system $f^*$ with $f_n$? 
We want to do so as well as possible based on the observations we have made up to the episode $n$. Ideally, we can continuously observe the system, but in practice, taking measurements is costly, which is why we want to take as few measurements as possible. Generally, we are free to query any number of points during an episode, which allows us to define a Measurement Selection Strategy (MSS). At each episode $n$, the MSS contains $m_n$ points at which we take measurements of the system. During an episode, at each time point $t_{n,i}$ in $i=\{1\ldots,m_n\}$, we measure the state-action pair $z_n(t_{n,i})=(x_n(t_{n,i},\pi_n(t_{n,i})))$ and the noisy state derivative $\dot{y}_n(t_{n,i})=\dot{x}_n(t_{n,i})+\epsilon_{n,i}$.

A natural MSS is the equidistant MSS, where at each episode we take $m_n$ equidistant measurements. Another option is to restrict $m_n$ to one query per episode, and then only take a measurement when we are most uncertain about the the dynamics of the executed policy, i.e. when the epistemic uncertainty $\sigma_n$ is the highest. We call this MSS the Oracle.

In our paper, we also present an adaptive MSS that is practical, i.e., easy to implement and at the same time requires only a few measurements per episode. The core idea is that we split each episode into $m_n$ intervals. At the beginning of every time interval, we measure the true state-action pair $z$, hallucinate with policy $\pi_n$ starting from $z$ for the duration of the interval, and select the time $t_{n,i}$ in the interval where the uncertainty on the hallucinated trajectory is the highest.

### OCORL: An Optimistic Exploration Algorithm
OCORL is a continuous-time variant of the H-UCRL strategy introduced by [reference: Chowdhury and Gopalan (2017); Curi et al. (2020)]. Recall that we are treating the case where the continuous-time dynamical system $f^*$ is unknown. 

So far, this gives us the following OC problem to solve at each episode $n$:

$$\pi_n = \argmin_{\pi_n \in \Pi} C(\pi_n, f_n) = \argmin_{\pi_n \in \Pi} \int_{0}^Tc(x_n, \pi(x_n)) \,dt$$
$$\text{s.t.} \quad \dot{x}_n = f_n(x_n, \pi_n(x_n)), \quad x_n(0) = x_0,$$

By deploying the policy $\pi_n$ on the real system after each episode, we get a trajectory $\mathcal{D}_n$. 

$$\mathcal{D}_n=\{ \text{Here: Either a mathematical or visual description of }\mathcal{D}_n\}$$

This leads us to the initial problem of when to measure the unknown discrete-time system. 

> Note: add how dataset is made from $\mathcal{D}_{1:n}$. 
Also add a visual description (see Lenarts Powerpoint).

The previously gathered data up to the current episode, denoted $\mathcal{D}_{1:n}$, gives us a basis from which we want to estimate the system dynamics. Similar to the OPAX setting before, the set of plausible models is given by $\mathcal{M}_{n}(\delta)$, which at each episode $n$ gives a region parametrized by the mean estimate $\mu_n$ and the epistemic uncertainty $\sigma_n$ in which the true system dynamics $f^*$ lie.

In other words, we thus say that with a probability of at least $1-\delta$, our true model lies within the intersection of all compatible model sets over all episodes up to $n$:

$$\Pr\bigg( f^* \in \bigcap_{n \ge 0}\mathcal{M_n}(\delta) \bigg) \geq 1-\delta.$$

>Note: Add a visual description of eqn above (Lenarts Powerpoint)

Now, for each episode $n$, we co-optimize over our goal and our model:

$$(\pi_n, f_n) = \arg \min_{\pi \in \Pi} \min_{f \in \mathcal{M}_{n-1} \cap \mathcal{F}} C(\pi, f).$$

$$\text{s.t.} \quad \dot{x}_n = f_n(x_n, \pi_n(x_n)), \quad x_n(0) = x_0,$$

> Note: There is a bug somewhere here that makes the rendering of the following EQNs unreadable on gitlab!

Here, $f_n$ is a dynamical system such that the cost by controlling $f_n$ with its optimal policy $\pi_n$ is the lowest among all the plausible systems after the previous iteration, given by the set $\mathcal{M}_{n-1}$. 
The additional constraint on $f$ given by the set $\mathcal{F}$ denotes the Lipschitz-continuity of $f^*$, which is a common assumption in control theory.
> (Maybe add a footnote: Assuming [Lipschitz-continuity](https://example.org) on our system dynamics means that we have no infinite slopes or discontinuities in our dynamics. Additionally, the derivative of our function has a continuous and bounded derivative. References: Khalil, 2015 and Curi, 2021).

With this method, we have reduced the problem to an optimal control problem, where we not only optimize over the policy, but also over plausible models of our system dynamics. This method is inherently optimistic, which drives exploration in a setting where the true dynamics $f^*$ is unknown.


### Theory
The co-optimization problem above is infinite-dimensional, in general nonlinear, and thus hard to solve $-$ or even completely intractable.
In our paper (Appendix B), we present details on how we solve it in practice using a reparametrization trick.

> Note: Maybe add some details about reparametrization trick.

For now, we assume that we can solve the OC problem. 
By doing so and deploying a policy $\pi_n$ at episode $n$ instead of the optimal policy $\pi^*$, we incur a regret,

$$r_n=C(\pi_n,f^*) - C(\pi^*,f^*).$$

We analyze OCORL using the notion of regret, which is a measure of the difference between the actual performance and the optimal performance under the best policy $\pi^*$ from the class $\Pi$. 
We evaluate the cumulative regret that sums the gaps between the performance of the policy $\pi_n$ and the optimal policy $\pi^*$ over all the episodes:

$$R_N = \sum_{n=1}^Nr_n.$$

If the cumulative regret $R_N$ is sublinear in $N$, then the average cost of the policy $C(\pi_n, f^*)$ converges to the optimal cost $C(\pi^*, f^*)$ [add footnote below]. 

> Footnote: When the cumulative regret $R_N$​ grows sublinearly with $N$, it means that as $N$ becomes large, the average regret per decision point $R_N/N$ tends to zero. 
This implies that the difference in performance between the policy being followed ($\pi_n$) and the optimal policy ($\pi$) diminishes over time.

In our paper, we show that when modeling the dynamics with Gaussian Processes (GP), the cumulative regret can be bounded and is sublinear in $N$ for common MSS choices. 
We do this by relating the regret bound to the following model complexity: 
$$\mathcal{I}_N(f^*, S) = \max_{\substack{\pi_1, \ldots, \pi_N \\ \pi_n \in \Pi}} \sum_{n=1}^N\int_0^T || \sigma_{n-1}(z_n(t))||^2 \,dt.$$

> Note: Maybe add notation from $\mathcal{I}_N$ to $\mathcal{MC}_N$ analoguous to OPAX part?

We expect that the regret of any model-based continuous-time RL algorithm depends both on the hardness of learning the underlying true dynamics model $f^*$ and the MSS. 
The model complexity captures both aspects, similarly to the model complexity notion in OPAX.

Intuitively, for a given $N$, the more complicated the dynamics $f^*$, the larger the epistemic uncertainty and thereby the model complexity. 
In the continuous-time setting, we do not observe the state at every time step, but only at a finite number of times wherever the MSS proposes to measure the system. 
Accordingly, the MSS influences how we collect data and update our calibrated model. 
Therefore, the model complexity depends on the MSS.

In our paper, we bound the model complexity for different MSS. Then, we prove that by running OCORL, we have with probability at least $1-\delta$ that the cumulative regret is bounded by the following:

$$R_N(S) \le 2 \beta_{N} L_c (1 + L_{\pi}) T^\frac{3}{2} e^{L_f (1 + L_{\pi}) T} \sqrt{N\mathcal{I}_N(f^*, S)}.$$

If the model complexity term $\mathcal{I}_N(f^*, S)$ and $\beta_{N}$ grow at a rate slower than $N$, the regret is sublinear and the average performance of OCORL converges to $C(\pi^*, f^*)$. 
We show that this is the case for different MSS by bounding the model complexity and modeling the dynamics with Gaussian processes (GP). 

To learn $f^*$, we fit a GP model with a zero mean and kernel $k$ to the collected data $\mathcal{D}_{1:n}$. 
We show sublinear regret for the proposed MSSs for the case when we model dynamics with GPs. 
Finally, the regret can be bounded as follows:

$$\textrm{TODO: Add regret bounds}$$

Here, $\gamma_n$ is the maximum information gain after observing $n$ points, like in the theoretical part of OPAX and introduced by [cite Srinivas].
For example, for the RBF kernel, $\gamma_n = \mathcal{O}\left(\log(n)^{d_x + d_u + 1}\right)$, where $d_x$ and $d_u$ refers to the state and input dimensionality.

To conclude, we related the regret bound $R_N$ to the model complexity $\mathcal{I}_N$, and then bounded the model complexity as well as the measurement uncertainty of GPs. 
This shows that the cumulative regret is sublinear in $N$, which assures convergence of our optimized policy $\pi_n$ to the optimal policy $\pi^*$. 
This theoretical framework lays the foundation for the practical application of OCORL, ensuring that the algorithm not only converges to optimal policies but does so efficiently, making it suitable for a wide range of continuous-time systems.


### Applications
> Mention some things. Maybe there is a nice figure?


# Conclusion
At NeurIPS 2023, we presented OPAX and OCORL, which both tackle the problem of exploration in unknown environments. OPAX relates principles from information gain to the model complexity, which gives convergence guarantees for general Bayesian models. This gives us a tractable objective function which drives exploration, allows us to learn the discrete system dynamics, and even converges to the true system dynamics for GPs.

OCORL starts from the notion of regret and relates it to the model complexity. We bound the model complexity as well as the measurement uncertainty, which assures convergence of our optimized policy to the optimal policy. By co-optimizing over the policy and the system, we also arrive at a model of the true dynamics.
