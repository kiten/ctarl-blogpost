<center>
<img id="fig1" class="alignnone size-large wp-image-6250" src="https://las.inf.ethz.ch/wp-content/uploads/2021/12/rhucrl-problem-setting.png" alt="" width="600" height="339" />
<b>Figure 1:</b><em> We propose an algorithm that learns a policy for continuous control tasks even when a worst-case adversary is present.</em></center>This is a post for the work <em>Combining Pessimism with Optimism for Robust and Efficient Model-Based Deep Reinforcement Learning</em> (Curi et al., 2021), jointly with Ilija Bogunovic and Andreas Krause, that appeared in ICML 2021.
It is a follow-up on <em>Efficient Model-Based reinforcement Learning Through Optimistic Policy Search and Planning</em> (Curi et al., 2020), that appeared in NeuRIPS 2020 <a href="https://berkenkamp.me/blog/2020-12-06-mbrl-exploration/">(See blog post)</a>
<h3>Introduction</h3>
In this work, we address the challenge of finding a robust policy in continuous control tasks.
As a motivating example, consider designing a braking system on an autonomous car.
As this is a highly complex task, we want to learn a policy that performs this maneuver.
One can imagine many real-world conditions and simulate them during training time e.g., road conditions, brightness, tire pressure, laden weight, or actuator wear.
However, it is unfeasible to simulate all such conditions and that might also vary in potentially unpredictable ways.
The main goal is to learn a policy that provably brakes in a robust fashion so that, even when faced with new conditions, it performs reliably.

Of course, the first key challenge is to model the possible conditions the system might be in.
Inspired by [latex]\mathcal{H}_\infty[/latex] control and Markov Games, we model unknown conditions with an adversary that interacts with the environment and the agent.
The adversary is allowed to execute adversarial actions at every instant by observing the current state and the history of the interaction. For example, if one wants to be robust to laden weight changes in the braking example, we allow the adversary to choose the laden weight.
By ensuring good performance with respect to the <strong>worst-case</strong> laden weight that an adversary selects, then we may also ensure good performance with respect to <strong>any other</strong> laden weight. Consequently, we say that <strong>such policy is robust</strong>.

The main algorithmic procedure is to train our agent together with a fictitious adversary that simulates the real-world conditions we want to be robust to.
The key question we ask is: <em>How to train these agents in a data-efficient way?</em>.
A common procedure is domain randomization (Tobin et al., 2020), in which the adversary is simulated by sampling at random from a domain distribution. This has proved useful for sim2real applications, but it has the drawback that it requires many domain-related samples, as easy samples are treated equally than hard samples, and it scales poorly with the dimensionality of the domain.
Another approach is RARL (Pinto et al., 2017), in which the adversary and the learner are trained with greedy gradient descent, i.e., without any exploration.
Although RARL performs well in some tasks, we demonstrate that the lack of exploration, particularly by the adversary, yields poor performance.
<h3>Problem Setting</h3>
In this section, we formalize the ideas from the introduction.
We consider a stochastic environment with states [latex]s[/latex], agent actions [latex]a[/latex], adversary actions [latex]\bar{a}[/latex], and i.i.d. additive transition noise vector [latex]\omega[/latex].
The dynamics are given by:

<center>[latex, display=True]
\begin{align}
s_{h+1} = f(s_h, a_h, \bar{a}_h) + \omega_h
\end{align}
[/latex]</center>We assume the true dynamics [latex]f[/latex] are <em>unknown</em> and consider the episodic setting over a finite time horizon [latex]H[/latex].
After every episode (i.e., every [latex]H[/latex] time steps), the system is reset to a known state [latex]s_0[/latex].
At every time-step, the system returns a reward [latex]r(s_h, a_h, \bar{a}_h)[/latex].
We consider time-homogeneous agent policies [latex]\pi[/latex], that select actions according to [latex]a_h = \pi(s_h)[/latex], as well as adversary policies [latex]\bar{\pi}[/latex] with [latex]\bar{a}_h = \bar{\pi}(s_h)[/latex].

We consider the performance of a pair of policies [latex](\pi, \bar{\pi})[/latex] on a given dynamical system [latex]\tilde{f}[/latex] as the episodic expected sum of returns:

<center>[latex, display=True]
\begin{align}
J(\tilde{f}, \pi, \bar{\pi}) &amp;:= \mathbb{E}_{\tau_{\tilde{f}, \pi, \bar{\pi}}}{ \left[ \sum_{h=0}^{H} r(s_h, a_h, \bar{a}_h) \, \bigg| \, s_0 \right]}, \quad
\text{s.t. Eq.(1).}
\end{align}
[/latex]</center>where [latex]\tau_{\tilde{f},\pi,\bar{\pi}}[/latex] is a random trajectory induced by the stochastic noise [latex]\omega[/latex], the dynamics [latex]\tilde{f}[/latex], and the policies [latex]\pi[/latex] and [latex]\bar{\pi}[/latex].

We use [latex]\pi^{\star}[/latex] to denote the optimal robust policy from the set [latex]\Pi[/latex] on the true dynamics [latex]f[/latex], i.e.,

<center>[latex, display=True]
\begin{align}
\pi^\star \in \arg \max_{\pi \in \Pi} \min_{\bar{\pi} \in \bar\Pi} J(f, \pi, \bar\pi).
\end{align}
[/latex]</center>For a small fixed [latex]\epsilon&gt;0[/latex], the goal is to output a robust policy [latex]\hat{\pi}_{T}[/latex] after [latex]T[/latex] episodes such that:

<center>[latex, display=True]
\begin{align}
\min_{\bar{\pi} \in \bar{\Pi}} J(f, \pi_T, \bar{\pi}) \geq \min_{\bar{\pi} \in \bar{\Pi}} J(f, \pi^{\star}, \bar{\pi}) - \epsilon,
\end{align}
[/latex]</center>Hence, we consider the task of near-optimal robust policy identification. Thus, the goal is to output the agent's policy with near-optimal robust performance when facing its own <strong>worst-case</strong> adversary, and the adversary selects [latex]\bar{\pi}[/latex] <strong>after</strong> the agent selects [latex]\pi_T[/latex].
This is a stronger robustness notion than just considering the worst-case adversary of the optimal policy, since, by letting [latex]\bar{\pi}^* \in \arg \min_{\bar{\pi} \in \bar\Pi} J(f, \pi^{\star}, \bar\pi)[/latex], we have [latex]J(f, \pi_T, \bar\pi^*) \geq \min_{\bar\pi \in \bar\Pi} J(f, \pi_T, \bar\pi)[/latex].
<h3>Algorithm: RH-UCRL</h3>
In this section, we present our algorithm, RH-UCRL. It is a model-based algorithm that explicitly uses the <strong>epistemic uncertainty</strong> in the model to explore, both for the agent and for the adversary.
In the next subsection, we will explain how we do the model-learning procedure, and in the following one, the policy optimization.
<h4>Model Learning</h4>
RH-UCRL is agnostic to the model one uses, as long as it is able to distinguish between epistemic and aleatoric uncertainty.
In particular, we build the set of all models that are <strong>compatible</strong> with the data collected up to episode [latex]t[/latex]. This set is defined as

<center>[latex, display=True]
\begin{align}
\mathcal{M}_t = \{\tilde{f}(z) \: \text{s.t.} \forall z \in \mathcal{Z} \: \| \tilde{f} (z) -\mu_{t-1} (z) \| \leq \beta_t \Sigma_{t-1}^{1/2}(z) \},
\end{align}
[/latex]</center>where [latex]\mu_{t-1}[/latex] is a mean function and [latex]\Sigma_{t-1}[/latex] a covariance function, and [latex]z = (s, a, a)[/latex] and [latex]\mathcal{Z} = \mathcal{S} \times \mathcal{A} \times \bar{\mathcal{A}}[/latex].
The set in (5) describes the epistemic uncertainty that we have about the system as it defines all the functions that are compatible with the data we collected so far.

Such set is parameterized by [latex]\beta_t[/latex] and it scales the confidence bounds so that the true model [latex]f[/latex] is contained in [latex]\mathcal{M}_t[/latex] with high probability for every episode.
For some classes of models, such as GP models, there are closed form expressions for [latex]\beta_t[/latex]. For deep neural networks, usually [latex]\beta_t[/latex] can be approximated via re-calibration.

In this work, we decided to use probabilistic ensembles of neural networks as in PETS (Chua et al., 2018) and H-UCRL (Curi et al., 2020).
We train each head of the ensemble using type-II MLE with one-step ahead transitions collected up to episode [latex]t[/latex]; we then consider the model output as a mixture of Gaussians. Concretely, each head predicts a Gaussian [latex]\mathcal{N}(\mu_{t-1}^{i}(z), \omega_{t-1}^{i}(z))[/latex]. We consider the mean prediction as [latex]\mu_{t-1}(z) = \frac{1}{N} \sum_{i=1}^N \mu_{t-1}^{i}(z)[/latex], the epistemic uncertainty as [latex]\Sigma_{t-1}(z) = \frac{1}{N-1} \sum_{i=1}^N (\mu_{t-1}(z) - \mu_{t-1}^{i}(z)) (\mu_{t-1}(z) - \mu_{t-1}^{i}(z))^\top[/latex], and the aleatoric uncertainty as [latex]\omega_{t-1}(z) = \frac{1}{N} \sum_{i=1}^N \omega_{t-1}^{i}(z)[/latex].
<h4>Policy Optimization</h4>
Given the set of plausible models [latex]\mathcal{M}_t[/latex] and a pair of agent and adversary policies [latex]\pi[/latex] and [latex]\bar \pi[/latex], we seek to construct <strong>optimistic</strong> and <strong>pessimistic</strong> estimates of the performance such policies by considering the epistemic uncertainty, which we denote as [latex]J_t^{(o)}(\pi, \bar \pi)[/latex] and [latex]J_t^{(p)}(\pi, \bar\pi)[/latex], respectively.
For example, the optimistic estimate is [latex]J_t^{(o)}(\pi, \bar \pi) = \max_{\tilde{f} \in \mathcal{M}_t} J(\tilde{f}, \pi, \bar{\pi})[/latex].
Unfortunately, optimizing w.r.t. the dynamics in the set [latex]\tilde{f}\in \mathcal{M}_t[/latex] is usually intractable, as it requires solving a constrained optimization problem.

Instead, we propose a variant of the reparameterization trick and introduce a function [latex]\eta(z): \mathcal{Z} \to [-1, 1]^{n}[/latex] and re-write the set of plausible models as

<center>[latex, display=True]
\begin{align} \mathcal{M}_t = \{\tilde f (z) \quad \text{s.t. } \exists \eta, \: \forall z, \; \tilde{f} = \mu_{t-1}(z) + \beta_t \Sigma_{t-1}^{1/2}(z) \eta(z) \}.\end{align}
[/latex]</center>Using the reparameterization in (6), the optimistic estimate is given by:

<center>[latex, display=True]
\begin{align}
J_t^{(o)} (\pi, \bar\pi) &amp;= \max_{\eta^o} J (f^{(o)}, \pi, \bar\pi)
\; \text{s.t.} \; f^o(\cdot) = \mu_{t-1}(\cdot) + \beta_{t} \Sigma_{t-1}^{1/2}(\cdot) \eta^o (\cdot).
\end{align}
[/latex]</center>Likewise, the pessimistic estimate is:

<center>[latex, display=True]
\begin{align}
J_t^{(p)} (\pi, \bar\pi) &amp;= \min_{\eta^p} J (f^{(p)}, \pi, \bar\pi)
\; \text{s.t.} \; f^p(\cdot) = \mu_{t-1}(\cdot) + \beta_{t} \Sigma_{t-1}^{1/2}(\cdot) \eta^p (\cdot).
\end{align}
[/latex]</center>Both (7) and (8) are non-linear finite-horizon optimal control problems, where the function [latex]\eta[/latex] is a <strong>hallucinated</strong> control that act linearly in the dynamics.
This <strong>hallucinated</strong> control is able to select a model from within the set [latex]\mathcal{M}_t[/latex] and directly controls the epistemic uncertainty that we currently have.
<h4>Final algorithm</h4>
The next step is to select the agent and adversary policies to deploy in the next episode. The R-HUCRL algorithm selects for the agent the policy that is <strong>most optimistic</strong> for any adversary policy, and for the adversary the policy that is <strong>most pessimistic</strong> for the selected agent policy. In equations, the algorithm is:

<center>[latex, display=True]
\begin{align}
\pi_t &amp;\in \arg\max_{\pi \in \Pi} \min_{\bar\pi \in \bar\Pi} J_{t}^{(o)}(\pi, \bar\pi), \\
\bar\pi_t &amp;\in \arg\min_{\bar\pi \in \bar\Pi} J_{t}^{(p)}(\pi_t, \bar\pi) .
\end{align}
[/latex]</center>Finally, after [latex]T[/latex] episodes, the algorithm returns the agent policy that had the largest pessimistic performance throughout the training phase,

<center>[latex, display=True]
\begin{align}
\hat{\pi}_T = \pi_{t^\star}\; \text{ s.t. } \; t^\star \in \arg\max_{t \in \{1, \ldots, T\} } J_{t}^{(p)}(\pi_t, \bar\pi_t).
\end{align}
[/latex]</center>Note that this quantity is computed in (11) and thus the last step has no extra computational cost.
<h3>Theory</h3>
The main theoretical contribution of our paper says that under some technical assumptions, and the number of episodes [latex]T[/latex] is large enough, i.e., when

<center>[latex, display=True]
\begin{align}
\frac{T}{\beta_{T}^{2H}\Gamma_{T}} \geq \frac{16L_r^2 H^3C^{2H}}{ \epsilon^2},
\end{align}
[/latex]</center>then, the output [latex]\hat{\pi}_T[/latex] of RH-UCRL in (11) satisfies

<center>[latex, display=True]
\begin{align}
\min_{\bar\pi \in \bar \Pi} J(f, \hat{\pi}_T, \bar \pi) \geq \min_{\bar\pi \in \Pi} J(f, \pi^{\star}, \bar \pi) - \epsilon
\end{align}
[/latex]</center>This is exactly the performance that we cared about in the problem setting in Equation (4).
So what are these terms in equation (12)? Will this condition ever hold?

The terms [latex]\beta_T[/latex] and [latex]\Gamma_T[/latex] are model-dependent and these quantify how hard is to learn the model. For example, for some Gaussian Process models, (e.g. RBF kernels), these are poly-logarithmic in [latex]T[/latex]. Thus, we expect that for these models and [latex]T[/latex] sufficiently large, condition (12) holds.
<h3>Applications</h3>
Finally, in the next subsections we show some application of the RH-UCRL algorithm.
As ablations, we propose some baselines derived from the algorithm:

- <strong>Best Response</strong>: It simply plays the pair of policies [latex](\pi, \bar \pi)[/latex] that optimize the optimistic performance, i.e., the solution to equation (9). This benchmark is intended to evaluate how pessimism explores in the space of adversarial policies.

- <strong>MaxiMin-MB/MaxiMin-MF</strong>: It simply plays the pair of policies [latex](\pi, \bar \pi)[/latex] that optimize the *expected* performance. As we don't explicitly use the epistemic uncertainty, we can do these either in a Model-Based or Model-Free way. This benchmark is intended to evaluate how hallucination helps to explore and to verify if there are big differences between model-based and model-free implementations.

- <strong>H-UCRL</strong>: We run H-UCRL, i.e., without an adversary. This benchmark is intended to evaluate how a non-robust algorithm performs in such tasks.

In the different applications, we also propose benchmarks that were specificly proposed for them and describe the evaluation procedure.
<h4>Parameter Robust Reinforcement Learning</h4>
This is possibly the most common application of robust reinforcement learning, in which the goal is to output a policy that has good performance uniformly over a set of parameters.
Getting back to the braking example, this setting can model, for example, different laden weights.
We apply RH-UCRL in this setting by considering state-independent policies.
Thus, RH-UCRL selects the most optimistic robust agent policy as well as the most pessimistic laden weight.

We consider Domain Randomization (Tobin et al., 2017) and EP-OPT (Rajeswaran et al., 2016) as related baselines designed for this setting.
Domain Randomization is an algorithm that optimizes the expected performance for the agent, and for the adversary it randomizes the parameters that one seeks to be robust to.
The randomization happens at the beginning of each episode.
EP-OPT is a refinement of Domain Randomization and only optimizes the agent on data generated in the worst [latex]\alpha[/latex]-percent of random episodes.

Next, we show the results of our experiments by varying the mass of different Mujoco environments between 0.1 and 2 times the original mass.

<center><img class="alignnone size-large wp-image-5863" src="https://las.inf.ethz.ch/wp-content/uploads/2021/11/parameter_robust.png" alt="" width="600" height="249" /></center><strong>Baseline Comparison:</strong>
We see that the performance of RH-UCRL is more stable compared to the other benchmarks and it is usually above the other algorithms. For the Swimmer environment, EP-OPT performs better, but in most other environments, it fails compared to RH-UCRL.

<strong>Ablation Discussion:</strong>
In the inverted pendulum, best response has very bad performance for a wide range of parameters. This is because early in the training it learns that the hardest mass is the smallest one, and then always proposes this mass, and fails to learn for the other masses.
As a non-robust ablation, H-UCRL constantly has lower performance than RH-UCRL.
Maximin-MF and Maximin-MB perform similarly in most environments, except in the hopper environment.
<h4>Adversarial Robust Reinforcement Learning</h4>
The final experiment in which we tested RH-UCRL is in a truly adversarial RL setting: We first train a policy using the different algorithms and then we allow an adversarial a second training phase for this fixed policy.
We refer to the performance of the policy in the presence of such adversary asthe <strong>worst-case</strong> return. Likewise, we refer to the performance of the policy without any adversary as the <strong>average</strong> return. This evaluation procedure is in contrast to what it is commonly done in the literature, in which an adversarial robust RL training procedure is employed, but only evaluated in the parameter-robust setting.

In the next figure we plot both metrics for the different algorithms previously discussed, as well as RARL (Pinto et al., 2017) and RAP (Vinitsky et al., 2020) baselines.

<center><img class="alignnone size-large wp-image-5863" src="https://las.inf.ethz.ch/wp-content/uploads/2021/11/adversarial_robust_bar.png" alt="" width="600" height="249" /></center>We first see that all algorithms have lower worst-case than average performance. However, the decrease of RH-UCRL performance is the least in all benchmarks. Furthermore, RH-UCRL usually has the <strong>highest</strong> worst-case performance, and also reasonably good <strong>average</strong> performance.
<h3>References</h3>
Chua, K., Calandra, R., McAllister, R., &amp; Levine, S. (2018). Deep reinforcement learning in a handful of trials using probabilistic dynamics models. Neural Information Processing Systems (NeurIPS), 4754–4765.

Curi, S., Berkenkamp, F., &amp; Krause, A. (2020). Efficient Model-Based Reinforcement Learning through Optimistic Policy Search and Planning. Neural Information Processing Systems (NeurIPS), 33.

Pinto, L., Davidson, J., Sukthankar, R., &amp; Gupta, A. (2017). Robust adversarial reinforcement learning. International Conference on Machine Learning, 2817–2826.

Vinitsky, E., Du, Y., Parvate, K., Jang, K., Abbeel, P., &amp; Bayen, A. (2020). Robust Reinforcement Learning using Adversarial Populations. ArXiv Preprint ArXiv:2008.01825.

Tobin, J., Fong, R., Ray, A., Schneider, J., Zaremba, W., &amp; Abbeel, P. (2017). Domain randomization for transferring deep neural networks from simulation to the real world. International Conference on Intelligent Robots and Systems (IROS), 23–30.

Rajeswaran, A., Ghotra, S., Ravindran, B., &amp; Levine, S. (2016). EPOpt: Learning Robust Neural Network Policies Using Model Ensembles. International Conference on Learning Representations, (ICLR).

Curi, S., Bogunovic, I., &amp; Krause, A. (2021). Combining Pessimism with Optimism for Robust and Efficient Model-Based Deep Reinforcement Learning. International Conference on Machine Learning (ICML).

This post is based on the following two papers:
[insert_php]
$_GET['bib'] ='laspub.bib';
$_GET['keys'] = '["curi21combining", "curi20hucrl"]';
include("bibtexbrowser.php");
[/insert_php]